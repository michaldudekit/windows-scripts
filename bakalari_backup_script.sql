DECLARE @TIMESTAMP varchar(255);
SELECT @TIMESTAMP = REPLACE(REPLACE(REPLACE(CONVERT(VARCHAR(19), CONVERT(DATETIME, getdate(), 112), 126), '-', ''), 'T', ''), ':', '') 

DECLARE @FILENAME varchar(255) = @TIMESTAMP+'_DB_Bakalari_Skolakrizik';
DECLARE @PATH varchar(500) = N'C:\backup\_Bakalari\'+@FILENAME+'.bak';

PRINT 'Zalohuji databazi: '+@FILENAME
PRINT 'Cesta umisteni zalohy je: '+@PATH

BACKUP DATABASE [bakalari] TO  DISK = @PATH WITH NOFORMAT, NOINIT,  NAME = N'bakalari-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
GO