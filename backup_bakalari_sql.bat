@ECHO OFF
ECHO [ RUN ] Spoustim zalohovani databaze Bakalari z MSSQL Serveru..
"C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\110\Tools\Binn\SQLCMD.EXE" -S WINSERVER\SQLEXPRESS -i "C:\backup\bakalari_backup_script.sql"

REM Nastaveni globalnich promennych pro zalohovani
SET BACKUFILE=C:\backup\_Bakalari\*.bak
SET BACKUPPATH=C:\backup\_Bakalari
setlocal
SET DATUM=%DATE:~-4,4%%DATE:~-8,2%%DATE:~-12,2%
SET CAS=%TIME:~-11,2%%TIME:~-8,2%%TIME:~-5,2%
SET FILENAME=%DATUM%%CAS%_DB_Bakalari_Skolakrizik.zip

ECHO [ RUN ] Spoustim archivaci .bak souboru se zalohou databaze..
cd %BACKUPPATH%
"C:\Program Files\7-Zip\7z.exe" a -sdel %FILENAME% %BACKUFILE%
